﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace WrightTrae_Code1
{
    public partial class Form1 : Form
    {

        //Trae Wright
        //8/30/17
        //MDV239
        //Code Assignment 1

        ListViewGroup lstGroupNeed = new ListViewGroup();
        ListViewGroup lstGroupHave = new ListViewGroup();

        //Inserts the two list groupsinot the list view
        public Form1()
        {
            InitializeComponent();
            lstGroupHave.Header = "Have";
            lstGroupNeed.Header = "Need";
            lstView.Groups.Insert(0, lstGroupNeed);
            lstView.Groups.Insert(1, lstGroupHave);
            
        }

        //adds the item inside the text field into the need group on the list
        private void btnAdd_Click(object sender, EventArgs e)
        {
            if (txtName.Text != "")//checks if the text box is empty
            {
                ListViewItem lvi = new ListViewItem();
                lvi.Text = txtName.Text;
                lvi.Group = lstGroupNeed;
                lstView.Items.Add(lvi);
                lstView.Enabled = true;
                btnSave.Enabled = true;
            }

        }

        //Switches the item over to the oposite list
        private void btnSwitch_Click(object sender, EventArgs e)
        {
            if (lstView.SelectedItems[0] != null)
            {
                if (lstView.SelectedItems[0].Group.Header == "Need")
                {
                    lstView.SelectedItems[0].Group = lstGroupHave;
                }
                else
                {
                    lstView.SelectedItems[0].Group = lstGroupNeed;
                }
                btnSwitch.Enabled = false;
                btnDelete.Enabled = false;
            }
        }

        //Makes the list controll buttons workable when you click on an item on the list and changes what the switch buttin says
        private void lstView_Click(object sender, EventArgs e)
        {
            if (lstView.SelectedItems.Count != 0)
            {
                if (lstView.SelectedItems[0].Group.Header == "Need")
                {
                    btnSwitch.Text = "Switch item to have";
                }
                else
                {
                    btnSwitch.Text = "Switch item to need";
                }
                btnSwitch.Enabled = true;
                btnDelete.Enabled = true;
            }
        }

        //delete the selected item from the list
        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (lstView.SelectedItems[0] != null)
            {
                lstView.SelectedItems[0].Remove();
                btnSwitch.Enabled = false;
                btnDelete.Enabled = false;
                if (lstView.Items.Count==0)
                {
                    btnSave.Enabled = false;
                    lstView.Enabled = false;
                }
            }

        }
        
        //exits the applictaion
        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        //saves the list to a txt document whereever the user chooses
        private void btnSave_Click(object sender, EventArgs e)
        {
            SaveFileDialog sfd = new SaveFileDialog();
            sfd.DefaultExt = ".txt";
            sfd.Filter = "txt files (*.txt)|*.txt|All files (*.*)|*.*";//Sets the fliter for what kind of files to search for
            if (sfd.ShowDialog() == DialogResult.OK)//Opens the dialog and waits for the user to click ok
            {
                using (StreamWriter sw = new StreamWriter(sfd.FileName))
                {
                    sw.WriteLine("Need:");
                    foreach (ListViewItem lv in lstGroupNeed.Items)
                    {
                        sw.WriteLine(lv.Text);
                    }

                    sw.WriteLine("\r\nHave:");
                    foreach (ListViewItem lv in lstGroupHave.Items)
                    {
                        sw.WriteLine(lv.Text);
                    }
                }
            }
        }
    }
}
