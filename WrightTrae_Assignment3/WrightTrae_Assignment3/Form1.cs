﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Xml;
using System.Xml.Linq;

namespace WrightTrae_Assignment3
{
    public partial class Form1 : Form
    {
        // NAME: Trae Wright
        // CLASS AND TERM: C201709
        // PROJECT: Tic Tac Toe

        /* THINGS TO CONSIDER:
            - You must change the project name to conform to the required
              naming convention.
            - You must comment the code throughout.  Failure to do so could result
              in a lower grade.
            - All button names and other provided variables and controls must
              remain the same.  Changing these could result in a 0 on the project.
            - Selecting Blue or Red on the View menu should change the imageList
              attached to all buttons so that any current play will change the color
              of all button images.
            - Saved games should save to XML.  A game should load only from XML and
              should not crash the application if a user tries to load an incorrect 
              file.
        */

        Button selectedButton;
        string turn="x";

        public Form1()
        {
            InitializeComponent();
        }

        //Changes the color of x and o's to blue
        private void blueToolStripMenuItem_Click(object sender, EventArgs e)
        {
            redToolStripMenuItem.Checked = false;
            blueToolStripMenuItem.Checked = true;
            r1c1button.ImageList = imlBlue;
            r1c2button.ImageList = imlBlue;
            r1c3button.ImageList = imlBlue;
            r2c1button.ImageList = imlBlue;
            r2c2button.ImageList = imlBlue;
            r2c3button.ImageList = imlBlue;
            r3c1button.ImageList = imlBlue;
            r3c2button.ImageList = imlBlue;
            r3c3button.ImageList = imlBlue;
        }

        //Changes the color of x and o's to red
        private void redToolStripMenuItem_Click(object sender, EventArgs e)
        {
            redToolStripMenuItem.Checked = true;
            blueToolStripMenuItem.Checked = false;
            r1c1button.ImageList = imlRed;
            r1c2button.ImageList = imlRed;
            r1c3button.ImageList = imlRed;
            r2c1button.ImageList = imlRed;
            r2c2button.ImageList = imlRed;
            r2c3button.ImageList = imlRed;
            r3c1button.ImageList = imlRed;
            r3c2button.ImageList = imlRed;
            r3c3button.ImageList = imlRed;
        }

        //selects the x to be played in the selected button
        private void xToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (selectedButton != null&&turn=="x")
            {
                turn = "o";
                if (selectedButton.ImageIndex == -1)
                {
                    selectedButton.ImageIndex = 1;
                }
                else
                {
                    MessageBox.Show("You can not edit this button, because it is already in use.");
                }
            }
            else
            {
                if (turn != "x")
                {
                    MessageBox.Show("It is O's turn.");
                }
                else
                {
                MessageBox.Show("You have to select a button before you can do this.");
                }

            }
            int winner;
            if (checkForWin(out winner)==true)
            {
                if (winner == 0)
                {
                    MessageBox.Show("The O's have won the game!!!");
                }
                else
                {
                    MessageBox.Show("The X's have won the game!!!");
                }
                toolStripButton1_Click(null, new EventArgs());
            }
        }

        //selects the o to be played in the selected button
        private void oToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (selectedButton != null&&turn=="o")
            {
                turn = "x";
                if (selectedButton.ImageIndex == -1)
                {
                    selectedButton.ImageIndex = 0;
                }
                else
                {
                    MessageBox.Show(".");
                }
            }
            else
            {
                if (turn != "o")
                {
                    MessageBox.Show("It is X's turn.");
                }
                else
                {
                    MessageBox.Show("You have to select a button before you can do this.");
                }
            }
            int winner;
            if (checkForWin(out winner) == true)
            {
                if (winner == 0)
                {
                    MessageBox.Show("The O's have won the game!!!");
                }
                else
                {
                    MessageBox.Show("The X's have won the game!!!");
                }
                toolStripButton1_Click(null, new EventArgs());
            }
        }

        //checks to see if anyone has won and if so it returns a boolean and the winner
        public Boolean checkForWin(out int winner)
        {
            int r1c1 = r1c1button.ImageIndex;
            int r3c3 = r3c3button.ImageIndex;
            int r2c2 = r2c2button.ImageIndex;
            winner = -1;
            if (r1c1 != -1)
            {
                if (r1c1 == r1c2button.ImageIndex && r1c1 == r1c3button.ImageIndex)//top row
                {
                    winner = r1c1;
                    return true;
                }
                if (r1c1 == r2c1button.ImageIndex && r1c1 == r3c1button.ImageIndex)//left column
                {
                    winner = r1c1;
                    return true;
                }
            }
            if (r3c3 != -1)
            {
                if (r3c3 == r3c2button.ImageIndex && r3c3 == r3c1button.ImageIndex)//bottom row
                {
                    winner = r3c3;
                    return true;
                }
                if (r3c3 == r2c3button.ImageIndex && r3c3 == r1c3button.ImageIndex)//right column
                {
                    winner = r3c3;
                    return true;
                }
            }
            if (r2c2!=-1)
            {
                if (r2c2 == r2c3button.ImageIndex && r2c2 == r2c1button.ImageIndex)//middle row
                {
                    winner = r2c2;
                    return true;
                }
                if (r2c2 == r1c2button.ImageIndex && r2c2 == r3c2button.ImageIndex)//middle column
                {
                    winner = r2c2;
                    return true;
                }

                if (r1c1 == r2c2 && r1c1 == r3c3)//left right diagonal
                {
                    winner = r1c1;
                    return true;
                }
                if (r2c2 == r3c1button.ImageIndex && r2c2 == r1c3button.ImageIndex)//right left diagonal
                {
                    winner = r2c2;
                    return true;
                }
            }
            return false;
        }

        //sets the selectedbutton to whichever buttton is clicked un
        private void r1c1button_Click(object sender, EventArgs e)
        {
            selectedButton = (Button)sender;
        }
        private void r1c2button_Click(object sender, EventArgs e)
        {
            selectedButton = (Button)sender;
        }
        private void r1c3button_Click(object sender, EventArgs e)
        {
            selectedButton = (Button)sender;
        }
        private void r2c1button_Click(object sender, EventArgs e)
        {
            selectedButton = (Button)sender;
        }
        private void r2c2button_Click(object sender, EventArgs e)
        {
            selectedButton = (Button)sender;
        }
        private void r2c3button_Click(object sender, EventArgs e)
        {
            selectedButton = (Button)sender;
        }
        private void r3c1button_Click(object sender, EventArgs e)
        {
            selectedButton = (Button)sender;
        }
        private void r3c2button_Click(object sender, EventArgs e)
        {
            selectedButton = (Button)sender;
        }
        private void r3c3button_Click(object sender, EventArgs e)
        {
            selectedButton = (Button)sender;
        }

        //Clears the board of x and o's
        public void toolStripButton1_Click(object sender, EventArgs e)
        {
            r1c1button.ImageIndex = -1;
            r1c2button.ImageIndex = -1;
            r1c3button.ImageIndex = -1;
            r2c1button.ImageIndex = -1;
            r2c2button.ImageIndex = -1;
            r2c3button.ImageIndex = -1;
            r3c1button.ImageIndex = -1;
            r3c2button.ImageIndex = -1;
            r3c3button.ImageIndex = -1;
        }

        //THIS FEATURE DOESNT WORK
        private void loadGameToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //XDocument doc = XDocument.Load("file.xml");
            //foreach (var img in doc.Descendants("img"))
            //{
            //    // src will be null if the attribute is missing
            //    string src = (string)img.Attribute("src");
            //    img.SetAttributeValue("src", src + "with-changes");
            //}
        }

        //THIS FEATURE DOESNT WORK
        private void saveGameToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //SaveFileDialog sfd = new SaveFileDialog();
            //sfd.DefaultExt = ".xml";
            //sfd.Filter = "xml files (*.xml)|*.xml|All files (*.*)|*.*";//Sets the fliter for what kind of files to search for
            //if (sfd.ShowDialog() == DialogResult.OK)//Opens the dialog and waits for the user to click ok
            //{


            //    XmlWriterSettings settings = new XmlWriterSettings();
            //    settings.Indent = true;
            //    using (XmlWriter writer = XmlWriter.Create(sfd.FileName, settings))
            //    {
            //        foreach (Control c in this.Controls)
            //        {
            //            if (c is Button)
            //            {
            //                Button b = (Button)c;
            //                writer.WriteString(b.Name);
            //            }
            //        }

            //    }
            //}
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
    }
}
