﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net.Mail;
using System.IO;

namespace WrightTrae_Assignment2
{
    public partial class Form1 : Form
    {
        //Trae Wright
        //9/6/17
        //MDV239
        //Code Assignment 2

        int editIndex;
        //Gets the contact info from the form and makes a listviewitem from it
        ListViewItem GetData
        {
            get
            {
                Contact con = new Contact();
                ListViewItem lvi = new ListViewItem();
                con.firstName = txtFName.Text;
                con.lastName = txtLName.Text;
                con.phoneNumber = txtPhoneNumber.Text;
                con.emailAddress = txtEmail.Text;
                if (rdoMale.Checked)
                {
                    con.gender = rdoMale.Text;
                    lvi.ImageIndex = 0;
                }
                else
                {
                    con.gender = rdoFemale.Text;
                    lvi.ImageIndex = 1;
                }
                lvi.Text = con.firstName + " " + con.lastName;
                lvi.Tag = con;
                return lvi;
            }
        }

        public Form1()
        {
            InitializeComponent();
        }

        //Inserts the contact into the listview if all of the fields are valid
        private void btnInsert_Click(object sender, EventArgs e)
        {
            if (Validator())
            {
                ListViewItem lvi = GetData;
                lsvContactList.Items.Add(lvi);
                ClearFields();
                btnDelete.Enabled = true;
                btnEdit.Enabled = true;
                btnSave.Enabled = true;
                lsvContactList.Enabled = true;
            }
            else
            {
                MessageBox.Show("Please check that you have filled in every blank \r\none of them is not in the correct format.");
            }

        }

        //Clears all fields
        public void ClearFields()
        {
            txtEmail.Clear();
            txtFName.Clear();
            txtLName.Clear();
            txtPhoneNumber.Clear();
            rdoMale.Checked = true;
        }

        //Validated the inputs for the contact information
        public Boolean Validator()
        {
            long temp;
            for (int i=0;i>10;i++)
            {
                if (txtFName.Text.Contains(i.ToString()) || txtLName.Text.Contains(i.ToString()))
                {
                    return false;
                }
            }
            if (!(string.IsNullOrWhiteSpace(txtFName.Text)&&string.IsNullOrWhiteSpace(txtLName.Text)&&string.IsNullOrWhiteSpace(txtFName.Text)&&string.IsNullOrWhiteSpace(txtLName.Text) &&Int64.TryParse(txtLName.Text, out temp)))
            {
                try
                {
                    MailAddress a = new MailAddress(txtEmail.Text);
                }
                catch
                {
                    return false;
                }
                return true;
            }
            else
            {
                return false;
            }
        }

        //Populates the fields with the data from the selected data and enables the buttons for editing
        private void btnEdit_Click(object sender, EventArgs e)
        {
            if (lsvContactList.SelectedItems.Count > 0)
            {
                Contact ct = (Contact)lsvContactList.SelectedItems[0].Tag;
                txtFName.Text = ct.firstName;
                txtLName.Text = ct.lastName;
                txtPhoneNumber.Text = ct.phoneNumber;
                txtEmail.Text = ct.emailAddress;
                if (ct.gender=="Male")
                {
                    rdoMale.Checked = true;
                }
                else
                {
                    rdoFemale.Checked = true;
                }
                gpbInfo.Text = "Edit";
                btnDone.Visible = true;
                btnCancel.Visible = true;
                btnInsert.Visible = false;
                editIndex = lsvContactList.SelectedItems[0].Index;
            }
            else
            {
                MessageBox.Show("Please select a contact to be edited.");
            }
        }

        //Updates the list if the inputs are validate
        private void btnDone_Click(object sender, EventArgs e)
        {
            if (Validate())
            {
                lsvContactList.Items[editIndex].Remove();
                lsvContactList.Items.Add(GetData);
                ClearFields();
                btnDone.Visible = false;
                btnCancel.Visible = false;
                btnInsert.Visible = true;
            }
            else
            {
                MessageBox.Show("Please check that you have filled in every blank \r\none of them is not in the correct format.");
            }
        }

        //Cancel the edit
        private void btnCancel_Click(object sender, EventArgs e)
        {
            ClearFields();
            btnDone.Visible = false;
            btnCancel.Visible = false;
            btnInsert.Visible = true;
        }

        //Deletes the currently selected item
        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (lsvContactList.SelectedItems.Count>0)
            {
                lsvContactList.Items[editIndex].Remove();
                if (lsvContactList.Items.Count==0)
                {
                    lsvContactList.Enabled = false;
                }
            }
            else
            {
                MessageBox.Show("Please select a contact to be deleted.");
            }
        }

        //saves the contact list to a text file where the users selects
        private void btnSave_Click(object sender, EventArgs e)
        {
            SaveFileDialog sfd = new SaveFileDialog();
            sfd.DefaultExt = ".txt";
            sfd.Filter = "txt files (*.txt)|*.txt|All files (*.*)|*.*";//Sets the fliter for what kind of files to search for
            if (sfd.ShowDialog() == DialogResult.OK)//Opens the dialog and waits for the user to click ok
            {
                using (StreamWriter sw = new StreamWriter(sfd.FileName))
                {
                    int count = 1;
                    sw.WriteLine("CONTACTS-");
                    foreach (ListViewItem lv in lsvContactList.Items)
                    {
                        Contact ct = (Contact)lv.Tag;
                        sw.WriteLine($"___Contact {count}___");
                        sw.WriteLine("Name: " +lv.Text);
                        sw.WriteLine("Phone Number: "+ct.phoneNumber);
                        sw.WriteLine("Email: " + ct.emailAddress);
                        sw.WriteLine("Gender: "+ct.gender);
                        count++;
                    }
                }
            }
        }

        //changes the icons to large icon
        private void largeIconToolStripMenuItem_Click(object sender, EventArgs e)
        {
            lsvContactList.View=View.LargeIcon;
            largeIconToolStripMenuItem.Checked = true;
            smallIconToolStripMenuItem.Checked = false;
        }

        //changes the icons to small icon
        private void smallIconToolStripMenuItem_Click(object sender, EventArgs e)
        {
            lsvContactList.View = View.SmallIcon;
            largeIconToolStripMenuItem.Checked = false;
            smallIconToolStripMenuItem.Checked = true;
        }
    }
}
